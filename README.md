# AWS WorkSpaces Test

This module demonstrates how to create a WorkSpace and WorkSpace directory using AWS WorkSpaces for user in the eu-central-1 (Frankfurt) through terraform.

## Note

The AWS WorkSpaces service requires an IAM role named `workspaces_DefaultRole`. If the role does not exist under IAM role prior to running terraform init, it would be created.

The IAM resources are defined in the Terraform source file [iam.tf](../modules/workspaces/iam.tf) The resources `aws_workspaces_directory.example` and `aws_workspaces_workspace.example` have dependencies on the IAM resources. The `depends_on` arguments calls for them.

## Running this build

This build would run by calling the commands bellow in the the `./test_env` environment;

```shell
terraform init
terraform plan
terraform apply -auto-approve
```

By default, resources are created in the `eu-central-1` region. To override the region, set the variable `aws_region` to a different value in the `variables.tf` which is in `./test_env.`
