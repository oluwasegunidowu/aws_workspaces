module "frankfurt" {
  source                           = "../modules/workspaces"
  iam_role_name                    = "workspaces_DefaultRole" # Default IAM role for AWS Workspaces
  #username                         = "Administrator"
  vpc_cidr_block                   = "10.0.0.0/16"
  subnet_a_cidr_block              = "10.0.1.0/24"
  subnet_b_cidr_block              = "10.0.2.0/24"
  ip_address_range1                = "10.10.10.10/16"
  ip_address_range2                = "11.11.11.11/16"
  directory_service_directory_name = "linux.workspace"
  az_region                        = "eu-central-1"
  zoneid                           = "euc1"
  #bundle_owner                     = "AMAZON"
  #bundle_name                      = "Standard with Amazon Linux 2"
  directory_edition                = "Standard"    # AWS workspaces directory edition
  directory_type                   = "MicrosoftAD" # AWS workspaces directory type
  password_length                  = 16
  secret_name                      = "workspace_secret"
}
