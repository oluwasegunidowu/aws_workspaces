variable "aws_region" {
  description = "The AWS region to use"
  default     = "eu-central-1"
}