variable "iam_role_name" {
  description = "Default Workspaces IAM role to created"
  type        = string
}

# variable "username" {
#   description = "The user name of the user for the WorkSpace. This user name must exist in the directory for the WorkSpace"
#   type        = string
# }

variable "vpc_cidr_block" {
  type = string
}

variable "subnet_a_cidr_block" {
  type = string
}

variable "subnet_b_cidr_block" {
  type = string
}

variable "directory_service_directory_name" {
  type = string
}

# variable "bundle_owner" {
#   description = "This is the owner of the bundle"
#   type        = string
# }

# variable "bundle_name" {
#   description = "This is the name of the bundle"
#   type        = string
# }

variable "az_region" {
  type = string
}

variable "zoneid" {
  type = string
}

variable "directory_edition" {
  description = "AWS workspaces directory edition"
  type        = string
}

variable "directory_type" {
  description = "The directory type"
  type        = string
}

variable "ip_address_range1" {
  description = "The directory type"
  type        = string
}

variable "ip_address_range2" {
  description = "The directory type"
  type        = string
}

variable "password_length" {
  description = "The length of the password"
  type        = string
  sensitive   = true
}

variable "secret_name" {
  description = "The name of the new secret."
  type        = string
}