resource "aws_workspaces_directory" "workspaces_directory" {
  directory_id = aws_directory_service_directory.directory.id       # The directory identifier for registration in WorkSpaces service
  subnet_ids   = [aws_subnet.private-a.id, aws_subnet.private-b.id] # The identifiers of the subnets where the directory resides

  self_service_permissions {
    change_compute_type  = true
    increase_volume_size = true
    rebuild_workspace    = true
    restart_workspace    = true
    switch_running_mode  = true
  }

  workspace_access_properties {
    device_type_android    = "DENY"
    device_type_chromeos   = "DENY"
    device_type_ios        = "DENY"
    device_type_linux      = "ALLOW"
    device_type_osx        = "ALLOW"
    device_type_web        = "DENY"
    device_type_windows    = "ALLOW"
    device_type_zeroclient = "DENY"
  }

  depends_on = [
    aws_iam_role.workspaces_default
  ]
}

# data "aws_workspaces_bundle" "standard_linux" {
# owner = var.bundle_owner # Owner of the bundle
 # name  = var.bundle_name  # The name of the bundle
#}



# resource "aws_workspaces_workspace" "workspace" {
#   directory_id = aws_workspaces_directory.workspaces_directory.id
#   bundle_id    = data.aws_workspaces_bundle.standard_linux.id
#   user_name    = var.username

#   workspace_properties {
#     compute_type_name                         = "VALUE"
#     user_volume_size_gib                      = 10
#     root_volume_size_gib                      = 80
#     running_mode                              = "AUTO_STOP"
#     running_mode_auto_stop_timeout_in_minutes = 60
#   }

#   tags = {
#     Environment = "Test"
#   }

#   depends_on = [
#     aws_iam_role_policy_attachment.workspaces-default-service-access
#   ]
# }

resource "aws_workspaces_ip_group" "ip_group" {
  name        = "main"
  description = "Main IP access control group"

  rules {
    source = var.ip_address_range1 # The IP address range, in CIDR notation
  }

  rules {
    source = var.ip_address_range2 # The IP address range, in CIDR notation
  }
}

data "aws_region" "current" {}

data "aws_availability_zones" "available_zone" {
  state = "available" # Allows to filter list of Availability Zones based on their current state.

  filter {
    name   = "opt-in-status"
    values = ["opt-in-not-required"]
  }
}

locals {
  region_workspaces_az_id_strings = {

    "${var.az_region}" = join(",", formatlist("${var.zoneid}-az%d", ["2", "3"]))
  }

  workspaces_az_id_strings = lookup(
    local.region_workspaces_az_id_strings,
    data.aws_region.current.name,
    join(",", data.aws_availability_zones.available_zone.zone_ids),
  )
  workspaces_az_ids = split(",", local.workspaces_az_id_strings)
}

resource "aws_vpc" "workspace_vpc" {
  cidr_block = var.vpc_cidr_block

  tags = {
    Name = "Workspace VPC"
  }
}

resource "aws_subnet" "private-a" {
  vpc_id               = aws_vpc.workspace_vpc.id
  availability_zone_id = local.workspaces_az_ids[0]
  cidr_block           = var.subnet_a_cidr_block

  tags = {
    Name = "Subnet A"
  }
}

resource "aws_subnet" "private-b" {
  vpc_id               = aws_vpc.workspace_vpc.id
  availability_zone_id = local.workspaces_az_ids[1]
  cidr_block           = var.subnet_b_cidr_block

  tags = {
    Name = "Subnet B"
  }
}

resource "aws_directory_service_directory" "directory" {
  name     = var.directory_service_directory_name
  password = random_password.password.result
  edition  = var.directory_edition
  type     = var.directory_type

  vpc_settings {
    vpc_id     = aws_vpc.workspace_vpc.id
    subnet_ids = [aws_subnet.private-a.id, aws_subnet.private-b.id]
  }
}
resource "random_password" "password" {
  length           = var.password_length # length of the random password to be created
  special          = true
  override_special = "_%@"
}

resource "aws_secretsmanager_secret" "secret" {
  name        = var.secret_name # name of the new secret.
  description = "Secret for storing password"
}

resource "aws_secretsmanager_secret_version" "secret_version" {
  secret_id     = aws_secretsmanager_secret.secret.id
  secret_string = random_password.password.result
}